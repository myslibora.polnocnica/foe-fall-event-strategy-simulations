import random

import numpy as np

from event_classes import choice_number
from event_info import *
from recipes import *

# Here define functions that take table of recipes available for a player and player's ingredients table (and possibly other parameters) and return which recipe to pick


def options(printing=None):  # returns list of choice_number (from event_info.py) of randomly chosen different recipes from recipe_set (from recipes.py)
    options_table = random.sample(tuple(recipe_set), choice_number)
    if printing:
        message = 'Player\'s options are:'
        for i in range(choice_number):
            message += f' {options_table[i].name},'
        print(message[:-1])  # [:-1] to get rid of the last comma
    return options_table


def can_bake(choices, ingds):  # returns False if player is unable to bake any recipe from given choice table
    bakeble = 0
    for recipe in choices:
        check = 1
        for i in range(len(ingredients)):
            if ingds[i] < recipe.ingds[i]:
                check = 0
        bakeble += check
    if bakeble == 0:
        return False
    else:
        return True


def use_max_ingds(choices, ingds,params=None):
    possible_recipes = [recipe for recipe in choices if can_bake([recipe], ingds)]
    if len(possible_recipes) == 0:
        raise ValueError('Error, not enough ingredients to bake any recipe')
    sorting_dict = {}  # sorting_dict with numbers of ingredients that player owns as keys and lists of ingredient indexes as values
    for ingd_index in range(len(ingds)):
        if ingds[ingd_index] in sorting_dict.keys():
            sorting_dict[ingds[ingd_index]].append(ingd_index)
        else:
            sorting_dict[ingds[ingd_index]] = [ingd_index]
    for ingredient_amount in reversed(sorted(list(sorting_dict.keys()))):  # we're iterating from highest number of ingredients to the lowest one
        remaining = []
        top_scores = -1
        indexes = sorting_dict[ingredient_amount]  # table of indexes of ingredients that player owns said number
        for recipe in possible_recipes:
            score = sum([recipe.ingds[i] for i in indexes])/recipe.stars()
            if score > top_scores:
                top_scores = score
                remaining = [recipe]
            elif score == top_scores:
                remaining += [recipe]
        possible_recipes = remaining
        if len(remaining) == 1:
            break
    return possible_recipes[0]


def no_use_min_ingds(choices, ingds,params=None):
    possible_recipes = [recipe for recipe in choices if can_bake([recipe], ingds)]
    if len(possible_recipes) == 0:
        raise ValueError('Error, not enough ingredients to bake any recipe')
    sorting_dict = {}  # sorting_dict with numbers of ingredients that player owns as keys and lists of ingredient indexes as values
    for ingd_index in range(len(ingds)):
        if ingds[ingd_index] in sorting_dict.keys():
            sorting_dict[ingds[ingd_index]].append(ingd_index)
        else:
            sorting_dict[ingds[ingd_index]] = [ingd_index]
    for ingredient_amount in sorted(list(sorting_dict.keys())):  # we're iterating from lowest number of ingredients to the highest one
        remaining = []
        top_scores = 2  # calculated score is always in range [0, 1]
        indexes = sorting_dict[ingredient_amount]  # table of indexes of ingredients that player owns said number
        for recipe in possible_recipes:
            score = sum([recipe.ingds[i] for i in indexes])/recipe.stars()
            if score < top_scores:
                top_scores = score
                remaining = [recipe]
            elif score == top_scores:
                remaining += [recipe]
        possible_recipes = remaining
        if len(remaining) == 1:
            break
    return possible_recipes[0]


def choose_by_tier_list(choices, ingds, params):  # tier list - dictionary with recipes as keys and their tier as value, strategy - strategy of choosing between recipes of the same tier
    if len(params) != 2:
        raise ValueError(f'Wrong shape of params = {params}')
    tier_list, strategy = params
    possible_recipes = [recipe for recipe in choices if can_bake([recipe], ingds)]
    if len(possible_recipes) == 0:
        raise ValueError('Error, not enough ingredients to bake any recipe')
    remaining = []
    top_scores = 1000
    for recipe in possible_recipes:  # loop picking highest tier recipes from choices
        if recipe not in tier_list.keys():
            raise ValueError('Error, recipe missing from tier list')
        score = tier_list[recipe]
        if score < top_scores:
            top_scores = score
            remaining = [recipe]
        elif score == top_scores:
            remaining += [recipe]
    if len(remaining) == 1:
        return remaining[0]
    else:
        return strategy(remaining, ingds)


def switcher_strategy(choices, ingds, params):  # params  = [lowest number of ingredient that we allow before switching, [strategy1 , strategy1_params], [strategy2, strategy2_params]]
    if params[0] < min(ingds):
        return params[1][0](choices, ingds, params[1][1])
    else:
        return params[2][0](choices, ingds, params[2][1])


