import numpy as np
import random
from game_info import *
from event_info import *


def check_prizes(prizes):  # takes table of tuples (prize name string, chance) and returns sum of chances of winning each prize (should equal 1)
    # rounded to two decimal places to fix floating point rounding errors in adding
    total = sum([x[1] for x in prizes])
    return np.around(total, decimals=2)


class Recipe:
    def __init__(self, name, ingds, prizes):  # ingds - table of numbers of specific ingredients in the order of ingredients table in event_info.py, prizes - table of tuples (prize, chance)
        self.name = name
        self.ingds = np.array(ingds)
        self.prizes = prizes
        self.chance_table = []  # table of numbers that divide [0, 1] range into sections of length equal to chances of getting respective prizes for picking random reward
        self.chance_table.append(self.prizes[0][1])
        for i in range(len(self.prizes) - 1):
            chance = self.prizes[i + 1][1]
            self.chance_table.append(np.around(self.chance_table[i] + chance, decimals=2)) # np.around function fixes floating point rounding error and doesn't mess correct numbers as long as they always have 2 decimal places
        check = check_prizes(self.prizes)
        if check != 1:
            raise ValueError(f'Error, sum of reward chances equals {check} instead of 1')

    def stars(self):
        return sum(self.ingds)

    def print_recipe(self):
        rec = ''
        for i in range(len(ingredients)):
            if self.ingds[i] > 0:
                name = ingredients[i]
                number = self.ingds[i]
                rec += f"{name}: {number}, "
        print(rec)

    def pick_reward(self): # returns random prize from prizes table using chance table to pick it randomly
        rand = random.uniform(0, 1)
        for i in range(len(self.chance_table)):
            if rand < self.chance_table[i]:
                return self.prizes[i][0]


class EventPrizes:  # class of all prizes available in the event initialized from recipes set
    def __init__(self, recipes, ad_values):  # recipes - set of all Recipe class objects from current event, ad_values - dictionary with prizes sellable in Antique Dealer as keys and [coin, minimum gems, maximum gems] as values
        self.forge_points = {} # dictionary of all Forge Points rewards with number of FPs as values
        self.medals = {}  # dictionary of all medal packs rewards with type (Gigantic, Extra Large etc) as values
        self.goods = {}  # dictionary of all goods rewards with number of goods as values
        self.blueprints = {}  # dictionary of all blueprints rewards with tuples (GB name or 'Random', number of blueprints) as values
        self.units = {}  # dictionary of all unit rewards with tuples (unit type, number of units) as values
        self.others = {}  # dictionary of fragments, buildings and consumables with AD [coin, minimum gems, maximum gems] table as values
        self.daily = {}  # dictionary of {daily:True} if daily prize exists in the event
        for recipe in recipes:
            for (prize, chance) in recipe.prizes:
                if 'Forge Point' in prize:
                    self.forge_points[prize] = int(prize.split()[0])
                elif 'Medals Pack' in prize:
                    self.medals[prize] = prize.split(' Medals')[0]
                elif 'Good' in prize:
                    self.goods[prize] = int(prize.split()[0])
                elif 'Blueprint' in prize:
                    name_parts = prize.split()
                    gb = ''
                    number = int(name_parts[0])
                    if 'Blueprint' in name_parts[1]:
                        gb = 'Random'
                    else:
                        for i in range(len(name_parts)-2):
                            gb += prize.split()[i + 1] + ' '
                    self.blueprints[prize] = (gb, number)
                elif 'Unit' in prize or 'Rouge' in prize or 'Champion' in prize or 'Colour Guard' in prize or 'Military Drummer' in prize:
                    number = 0
                    unit_type = ''
                    name_parts = prize.split()
                    if name_parts[0].isnumeric():
                        number = int(name_parts[0])
                        unit_type = name_parts[1:]
                    else:
                        number = 1
                        unit_type = prize
                    if unit_type[-1] == 's':  # normalizing unit names to singular
                        unit_type = unit_type[:-1]
                    self.units[prize] = (unit_type, number)
                elif 'daily' in prize:
                    self.daily[prize] = True
                else:
                    if prize not in ad_values.keys():
                        raise ValueError(prize + ' not in Antique Dealer value dictionary or any other prize type, check your prize list')
                    else:
                        self.others[prize] = np.array(ad_values[prize])

# below methods take as argument dictionary of all prizes the player has won as keys and numbers of said prizes as values and return various statistics

    def fp_sum(self, player_prizes):
        total = 0
        for prize in self.forge_points.keys():
            if prize in player_prizes.keys():
                total += self.forge_points[prize] * player_prizes[prize]
        return total

    def goods_sum(self, player_prizes):
        total = 0
        for prize in self.goods.keys():
            if prize in player_prizes.keys():
                total += self.goods[prize] * player_prizes[prize]
        return total

    def medals_sum(self, player_prizes, age):
        total = 0
        for prize in self.medals.keys():
            if prize in player_prizes.keys():
                pack_type = self.medals[prize]
                total += medals_number(pack_type, age) * player_prizes[prize]
        return total

    def blueprints_sum(self, player_prizes):  # returns dictionary with Great Building name (or 'Random' string) as keys and numbers of specific blueprints won as values
        total = {}
        for prize in self.blueprints.keys():
            if prize in player_prizes.keys():
                gb_type = self.blueprints[prize][0]
                number = self.blueprints[prize][1] * player_prizes[prize]
                if gb_type in total.keys():
                    total[gb_type] += number
                else:
                    total[gb_type] = number
        return total

    def all_blueprints_sum(self, player_prizes):
        bps = self.blueprints_sum(player_prizes)
        total = 0
        for gb_type in bps.keys():
            total += bps[gb_type]
        return total

    def units_sum(self, player_prizes):
        total = {}
        for prize in self.units.keys():
            if prize in player_prizes.keys():
                unit_type = self.units[prize][0]
                number = self.units[prize][1] * player_prizes[prize]
                if unit_type in total.keys():
                    total[unit_type] += number
                else:
                    total[unit_type] = number
        return total

    def standard_units_sum(self, player_prizes):
        unit_sum = self.units_sum(player_prizes)
        total = 0
        for unit_type in unit_sum.keys():
            if 'Unit' in unit_type:
                total += unit_sum[unit_type]
        return total

    def other_special_units(self, player_prizes):  # sum of Colour Guards and Military Drummers
        unit_sum = self.units_sum(player_prizes)
        total = 0
        for unit_type in unit_sum.keys():
            if 'Drummer' in unit_type or 'Guard' in unit_type:
                total += unit_sum[unit_type]
        return total

    def other_sum(self, player_prizes, wanted=None):  # wanted - table of names of other rewards that player isn't going to sell to Antique Dealer
        if wanted is None:
            wanted = []
        value = np.array([0.0, 0.0, 0.0])
        wanted_won = {}  # dictionary of wanted prizes as keys and number of them won as vales
        for prize in self.others.keys():
            if prize in player_prizes.keys():
                if prize in wanted:
                    wanted_won[prize] = player_prizes[prize]
                else:
                    added_value = player_prizes[prize]*self.others[prize]
                    value += added_value
        return value, wanted_won



