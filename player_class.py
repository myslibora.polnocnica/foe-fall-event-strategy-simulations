from strategies import *
from event_info import *
import numpy as np


class Player:
    def __init__(self, name, ingds, age, wanted):  # wanted - a table of other prizes (consumables, buildings, fragments) that player is going to keep
        self.name = name
        self.ingds = np.array(ingds)
        self.age = age
        self.prizes = {}  # dictionary, prize name as keys and number of said prizes won as values
        self.simple_daily = 0
        self.wanted = wanted
        self.stars = 0

    def bake(self, recipe, printing=None):
        if len(self.ingds) != len(recipe.ingds):
            raise ValueError('Error, invalid recipe ingredients or player ingredients')
        for i in range(len(self.ingds)):
            if recipe.ingds[i] > self.ingds[i]:
                raise ValueError('Error, not enough ingredients to make this recipe')
        self.ingds -= recipe.ingds
        reward = recipe.pick_reward()
        for prize in recipe.prizes:
            if prize[0] == 'daily':
                self.simple_daily += prize[1]

        if reward in self.prizes.keys():
            self.prizes[reward] += 1
        else:
            self.prizes[reward] = 1
        self.stars += recipe.stars()
        if printing:
            print(f'The player has baked {recipe.name} and won {reward}')


    def execute_strategy(self, strategy, strategy_argument, printing=None):
        if printing:
            print(f'Player\'s starting ingredients are: {ingredients}: {self.ingds}')
        choice = options(printing)
        while can_bake(choice, self.ingds):
            baked_recipe = strategy(choice, self.ingds, strategy_argument)
            self.bake(baked_recipe, printing)
            choice = options(printing)
        if printing:
            print(f'Leftover ingredients are: {ingredients}: {self.ingds}')
