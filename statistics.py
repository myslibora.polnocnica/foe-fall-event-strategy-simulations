import matplotlib.pyplot as plt
from strategies import *
from player_class import Player


def evaluate_strategy(strategy, strategy_arguments, wanted, age, sample_size, event_prizes, ingd_function=get_ingredients,
        printing=None,drawing=None):  # strategy argument - table of all argument needed for strategy, ingd_function - function of randomizing
    # player's ingredients,
    daily_table = []
    daily_table2 = []
    players_stars = []
    forge_points = []
    medals = []
    blueprints = []
    units = []
    ad_currency = []
    wanted_table = []
    for i in range(len(wanted)):
        wanted_table.append((wanted[i], []))
    for i in range(sample_size):
        sample_player = Player(f'Player {i}', ingd_function(), age, wanted)
        sample_player.execute_strategy(strategy, strategy_arguments)
        daily_table.append(sample_player.prizes.get('daily', 0))
        daily_table2.append(sample_player.simple_daily)
        players_stars.append(sample_player.stars)
        forge_points.append(event_prizes.fp_sum(sample_player.prizes))
        medals.append(event_prizes.medals_sum(sample_player.prizes, age))
        blueprints.append(event_prizes.blueprints_sum(sample_player.prizes))
        units.append(event_prizes.units_sum(sample_player.prizes))
        ad_currency.append(event_prizes.other_sum(sample_player.prizes, wanted))
        for j in range(len(wanted_table)):
            wanted_table[j][1].append(sample_player.prizes.get(wanted_table[j][0], 0))

    if printing is not None:
        report_daily = f'Using this strategy, a player will get at average {np.average(daily_table)} ± {np.std(daily_table)} daily reward,\n'
        # report_daily2 = f'Using this strategy, a player will get at average {np.average(daily_table2)} ± {np.std(daily_table2)} daily reward2,'
        report_stars = f'Using this strategy, a player will get at average {np.average(players_stars)} ± {np.std(players_stars)} stars,\n'
        report_fp = f'Using this strategy, a player will get at average {np.average(forge_points)} ± {np.std(forge_points)} forge points,\n'
        report_medals = f'Using this strategy, a player will get at average {np.average(medals)} ± {np.std(medals)} medals,\n'

        ad_coins = [element[0][0] for element in ad_currency]
        ad_gems_min = [element[0][1] / 2 for element in ad_currency]
        ad_gems_max = [element[0][2] / 2 for element in ad_currency]

        report_ad_currency = f'Using this strategy, a player will get at average {np.average(ad_coins)} ± {np.std(ad_coins)} coins, {np.average(ad_gems_min)} ± {np.std(ad_gems_min)} gems at min ' \
                             f'and {np.average(ad_gems_max)} ± {np.std(ad_gems_max)}\n'

        all_blueprint_names = set([name_count[0] for name_count in event_prizes.blueprints.values()])
        report_blueprints = f'Using this strategy, a player will get:'
        for name in all_blueprint_names:
            blueprints_for_name = [dict_blueprint[name] if name in dict_blueprint.keys() else 0 for dict_blueprint in blueprints]
            report_blueprints += f"\n   at average {np.average(blueprints_for_name)} ± {np.std(blueprints_for_name)} blueprint of {name}"

        all_units_names = set(name_count[0] for name_count in event_prizes.units.values())
        report_units = f'\nUsing this strategy, a player will get :'
        for name in all_units_names:
            units_for_name = [dict_units[name] if name in dict_units.keys() else 0 for dict_units in units]
            report_units += f"\n    at average {np.average(units_for_name)} ± {np.std(units_for_name)} units of type {name}"
        printing(report_daily)
        # printing(report_daily2)
        printing(report_stars)
        printing(report_fp)
        printing(report_medals)
        printing(report_ad_currency)

        printing(report_blueprints)
        printing(report_units)

        if len(wanted) != 0:
            report_wanted = "\nUsing this strategy, a player will get at:"
            for wanted_element in wanted_table:
                report_wanted += f'\n   average {np.average(wanted_element[1])} ± {np.std(wanted_element[1])} {wanted_element[0]}'
            printing(report_wanted)

        if drawing is not None:
            hist_bins = np.array((range(min(daily_table), max(daily_table))))
            plt.xticks(hist_bins)
            plt.hist(daily_table, bins=hist_bins + 0.5, label="Daily rewards", rwidth=0.9)
            plt.legend(loc="upper left")
            plt.gcf().savefig(f"res/daily_{drawing}.png")
            plt.clf()
    wanted_results = {}
    for wanted_element in wanted_table:
        wanted_results[wanted_element[0]] = np.average(wanted_element[1])

    return np.average(daily_table), np.average(players_stars), wanted_results
