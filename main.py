from event_classes import *
from player_class import *
from statistics import evaluate_strategy
from tier_lists import *
import sys

sample_size = 50000

fall_event_prizes = EventPrizes(recipe_set, ad_values)
if sys.argv[1] == "balance":  # checking which balancing strategy is optimal, if False/if True instead of # comment for easier modifications
    report = open("res/raport_balance", "w")
    report.write(f"\n\n== no_use_min_ingds ==\n\n")
    evaluate_strategy(no_use_min_ingds, [], [], 'Iron Age', sample_size, fall_event_prizes, printing=report.write, drawing="no_use_min_ingds")
    report.write(f"\n\n== use_max_ingds ==\n\n")
    evaluate_strategy(use_max_ingds, [], [], 'Iron Age', sample_size, fall_event_prizes, printing=report.write, drawing="use_max_ingds")

better_balancer = no_use_min_ingds
print("Balanced used is no_use_min_ingds")
if sys.argv[1] == "tier":  # checking what tier list is best, with and without balancer
    report = open("res/raport_tiers", "w")
    tier_lists = [tier_by_chance_per_ingds, tier_by_loss_per_recipe, tier_by_loss_per_ingds]
    tier_lists_names = ["tier_by_chance_per_ingds", "tier_by_loss_per_recipe", "tier_by_loss_per_ingds"]

    for tier_list, name in zip(tier_lists, tier_lists_names):
        report.write(f"\n\n== {name} without switching ==\n\n")
        evaluate_strategy(choose_by_tier_list, [tier_list, better_balancer], [], 'Early Middle Ages', sample_size, fall_event_prizes, printing=report.write, drawing=name)
    switch_value = 5

    for tier_list, name in zip(tier_lists, tier_lists_names):
        report.write(f"\n\n== {name} with switching at {switch_value}==\n\n")
        evaluate_strategy(switcher_strategy, [switch_value, [choose_by_tier_list, [tier_list, better_balancer]], [better_balancer, []]], [], 'Early Middle Ages', sample_size, fall_event_prizes,printing=report.write, drawing=name+"switch")

best_tier_list = tier_by_loss_per_recipe

if sys.argv[1] == "whycutof":  # plotting for optimal tier list and balancer performance as a function of cutoff
    cutoff_table = []
    daily_table = []
    stars_table = []
    for cutoff in range(-1, 15):
        daily, stars, _ = evaluate_strategy(switcher_strategy, [cutoff, [choose_by_tier_list, [best_tier_list, better_balancer]], [better_balancer, []]], [], 'Early Middle Ages', sample_size//5, fall_event_prizes)
        cutoff_table.append(cutoff)
        daily_table.append(daily)
        stars_table.append(stars)

    import matplotlib.pyplot as plt

    fig, ax1 = plt.subplots()

    color = 'tab:red'
    ax1.set_xlabel('Cutoff')
    ax1.set_ylabel('Daily', color=color)
    ax1.plot(cutoff_table, daily_table, color=color)
    ax1.tick_params(axis='y', labelcolor=color)

    ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis

    color = 'tab:blue'
    ax2.set_ylabel('Stars', color=color)  # we already handled the x-label with ax1
    ax2.plot(cutoff_table, stars_table, color=color)
    ax2.tick_params(axis='y', labelcolor=color)

    fig.tight_layout()  # otherwise the right y-label is slightly clipped
    plt.legend(loc="upper left")
    plt.gcf().savefig(f"res/cutoff_reason.png")
    plt.clf()

optimal_cutoff = 5

if False:  # finding optimal tier list for ww
    better_ww = {}
    wanted = ['Fragment of Wishing Well', 'Fragment of Wishing Well Shrink Kit', ]
    for key in tier_for_wells.keys():
        better_ww[key] = best_tier_list[key] + 50 * tier_for_wells[key]
    print("tier_for_wells")
    evaluate_strategy(switcher_strategy, [optimal_cutoff, [choose_by_tier_list, [tier_for_wells, better_balancer]], [better_balancer, []]], wanted, 'Early Middle Ages', sample_size, fall_event_prizes)
    print("better_ww")
    evaluate_strategy(switcher_strategy, [optimal_cutoff, [choose_by_tier_list, [better_ww, better_balancer]], [better_balancer, []]], wanted, 'Early Middle Ages', sample_size, fall_event_prizes)
if False:
    print("optimal")
    evaluate_strategy(switcher_strategy, [optimal_cutoff, [choose_by_tier_list, [best_tier_list, better_balancer]], [better_balancer, []]], [], 'Early Middle Ages', sample_size, fall_event_prizes, printing=True, drawing=True)
