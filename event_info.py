import random
ingredients = ['apple', 'pumpkin', 'chocolate', 'cinnamon', 'caramel']

# During a turn, we have choice_number of recipes available and we choose one of them.
choice_number = 3

base = [26, 27, 27, 27, 26]  # number of each ingredient obtained from quest lines and login bonuses
days = 20  # number of event days


def get_ingredients():
    ingds = base.copy()
    for i in range(days): # adding 2 random ingredients from daily chest for each event day
        ingd_index = random.randint(0, 4)
        ingds[ingd_index] += 2
    return ingds

