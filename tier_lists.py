from recipes import *

tier_by_chance_per_ingds = {
}

for recipe in recipe_set:
    if recipe.prizes[0][0] != 'daily':
        raise ValueError(f'Error, daily prize not first on the prize list for {recipe.name}')
    daily_chance = recipe.prizes[0][1]
    new_tier = 1000 - int(1000.0*(daily_chance/recipe.stars()))
    tier_by_chance_per_ingds[recipe] = new_tier


tier_by_loss_per_recipe = {
    applePie: 3,
    pumpkinCobbler: 4,
    chocolateRolls: 7,
    pumpkinPie: 5,
    chocolateCake: 6,

    pumpkinCheesecake: 2,
    pumpkinMuffins: 2,
    chocolateBrownies: 3,
    chocolateBread: 5,
    caramelApple: 2,

    appleJams: 1,
    pumpkinCookies: 1,
    snickerDoodles: 0,
    chocolateCookies: 0,
    caramelBites: 1,
}

tier_by_loss_per_ingds = {
    applePie: 3,
    pumpkinCobbler: 4,
    chocolateRolls: 8,
    pumpkinPie: 6,
    chocolateCake: 7,

    pumpkinCheesecake: 2,
    pumpkinMuffins: 2,
    chocolateBrownies: 5,
    chocolateBread: 9,
    caramelApple: 2,

    appleJams: 1,
    pumpkinCookies: 1,
    snickerDoodles: 0,
    chocolateCookies: 0,
    caramelBites: 1,
}


tier_for_wells = {  # focused on getting as many Wishing Wells and WW Shrink kits as possible, 0 - recipe that gives WW fragments, 1 - recipe that doesn't use ingredients used for 0, 2 - recipe that uses 1 needed
    applePie: 1,
    pumpkinCobbler: 1,
    chocolateRolls: 2,
    pumpkinPie: 1,
    chocolateCake: 2,

    pumpkinCheesecake: 2,
    pumpkinMuffins: 1,
    chocolateBrownies: 2,
    chocolateBread: 1,
    caramelApple: 2,

    appleJams: 1,
    pumpkinCookies: 1,
    snickerDoodles: 1,
    chocolateCookies: 1,
    caramelBites: 0,
}