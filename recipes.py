from event_classes import Recipe

applePie = Recipe('Apple Pie', [2, 0, 0, 1, 0], [('daily', 0.15), ('Gigantic Medals Pack', 0.18), ('Extra Large Medals Pack', 0.28), ('Large Medals Pack', 0.24), ('Medium Medals Pack', 0.15)])
pumpkinCobbler = Recipe('Pumpkin Apple Cobbler', [1, 1, 0, 1, 0], [('daily', 0.15), ('50 Goods', 0.35), ('25 Goods', 0.38), ('10 Goods', 0.12)])
chocolateRolls = Recipe('Chocolate Caramel Cinnamon Rolls', [0, 0, 1, 1, 1], [('daily', 0.10), ['Mass Self-Aid Kit', 0.18], ['Store Building', 0.24], ['Boost Crate', 0.24], ('Self-Aid Kit', 0.24)])
pumpkinPie = Recipe('Pumpkin Pie', [0, 2, 0, 1, 0], [('daily', 0.14), ('20 Forge Points', 0.36), ('10 Forge Points', 0.30), ('5 Forge Points', 0.20)])
chocolateCake = Recipe('Chocolate Cake', [0, 0, 2, 0, 1], [('daily', 0.14), ('6h Mass Supply Rush', 0.24), ('1h Mass Coin Rush', 0.24), ('30m Mass Supply Rush', 0.38)])

pumpkinCheesecake = Recipe('Pumpkin Caramel Cheesecake', [0, 1, 0, 0, 1], [('daily', 0.14), ('100% Supply Boost', 0.22), ('100% Coin Boost', 0.22), ('50% Supply Boost', 0.21), ('50% Coin Boost', 0.21)])
pumpkinMuffins = Recipe('Pumpkin Apple Muffins', [1, 1, 0, 0, 0], [('daily', 0.13), ('Premium Decoration', 0.21), ('Residential Building', 0.22), ('Production Building', 0.22), ('Cultural Building', 0.22)])
chocolateBrownies = Recipe('Chocolate Fudge Brownies', [0, 0, 1, 0, 1], [('daily', 0.10), ('2 Blueprints', 0.24), ('1 Observatory Blueprint', 0.28), ('1 Blueprint', 0.38)])
chocolateBread = Recipe('Chocolate Cinnamon Bread', [0, 0, 1, 1, 0], [('daily', 0.08), ('10% Attacker Boost', 0.24), ('5% Attacker Boost', 0.22), ('30% Defender Boost', 0.24), ('20% Defender Boost', 0.22)])
caramelApple = Recipe('Caramel Apple', [1, 0, 0, 0, 1], [('daily', 0.12), ('Rouge', 0.35), ('Champion', 0.35), ('Colour Guard', 0.09), ('Military Drummer', 0.09)])

appleJams = Recipe('Apple Jams', [1, 0, 0, 0, 0], [('daily', 0.06), ('10 Forge Points', 0.32), ('5 Forge Points', 0.40), ('2 Forge Points', 0.22)])
pumpkinCookies = Recipe('Pumpkin Spice Cookies', [0, 1, 0, 0, 0], [('daily', 0.08), ('20 Goods', 0.28), ('10 Goods', 0.32), ('5 Goods', 0.32)])
snickerDoodles = Recipe('Snickerdoodles', [0, 0, 0, 1, 0], [('daily', 0.10), ('Fast Unit', 0.18), ('Heavy Unit', 0.18), ('Light Unit', 0.18), ('Ranged Unit', 0.18), ('Artillery Unit', 0.18)])
chocolateCookies = Recipe('Chocolate Chip Cookies', [0, 0, 1, 0, 0], [('daily', 0.10), ('Large Medals Pack', 0.30), ('Medium Medals Pack', 0.30), ('Small Medals Pack', 0.30)])
caramelBites = Recipe('Caramel Bites', [0, 0, 0, 0, 1], [('daily', 0.07), ('Fragment of Shrine of Knowledge', 0.31), ('Fragment of Wishing Well', 0.31), ('Fragment of Wishing Well Shrink Kit', 0.31)])

recipe_set = {applePie, pumpkinCobbler, chocolateRolls, pumpkinPie, chocolateCake, pumpkinCheesecake, pumpkinMuffins, chocolateBrownies, chocolateBread, caramelApple, appleJams, pumpkinCookies, snickerDoodles, chocolateCookies, caramelBites}
