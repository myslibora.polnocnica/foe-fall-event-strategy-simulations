# Function returning number of medals in a pack based on player's age doesn't work for Space Age: Venus due to lacking data

multipliers = {
    'Bronze Age': 1,
    'Iron Age': 2,
    'Early Middle Ages': 4,
    'High Middle Ages': 8,
    'Industrial Age': 40,
    'Progressive Era': 60,
    'Modern Era': 90,
    'Contemporary Era': 160,
    'Tomorrow Era': 220,
    'Future Era': 240,
    'Arctic Future': 260,
    'Oceanic Future': 280,
    'Space Age Mars': 360
}

bronze_values = {
    'Gigantic': 150,
    'Extra Large': 100,
    'Large': 50,
    'Medium': 25,
    'Small': 10,
}

lma_values = {
    'Gigantic': 2500,
    'Extra Large': 1400,
    'Large': 700,
    'Medium': 350,
    'Small': 150,
}

colonial_values = {
    'Gigantic': 3600,
    'Extra Large': 2400,
    'Large': 1200,
    'Medium': 600,
    'Small': 250,
}

postmodern_values = {
    'Gigantic': 18000,
    'Extra Large': 1200,
    'Large': 6000,
    'Medium': 3000,
    'Small': 1250,
}

vf_values = {
    'Gigantic': 46000,
    'Extra Large': 31000,
    'Large': 15500,
    'Medium': 7750,
    'Small': 3200,
}


def medals_number(pack_type, age):
    if pack_type not in bronze_values.keys():
        raise ValueError("Error, invalid Medals Pack type")
    if age in multipliers.keys():
        return bronze_values[pack_type]*multipliers[age]
    elif age == 'Late Middle Ages':
        return lma_values[pack_type]
    elif age == 'Colonial Age':
        return colonial_values[pack_type]
    elif age == 'Postmodern Era':
        return postmodern_values[pack_type]
    elif age == 'Virtual Future':
        return vf_values[pack_type]
    else:
        raise ValueError("Error, invalid Era")


# Fragments can't be sold in Antique Dealer, only full building, but for the sake of analysis I treat them like they are worth (1/15) of the building selling prize
ad_values = {
    'Self-Aid Kit': [140, 0, 0],
    'Mass Self-Aid Kit': [1220, 0, 1],
    'Store Building': [1350, 0, 1],
    'Boost Crate': [510, 0, 0],
    '6h Mass Supply Rush': [1500, 0, 0],
    '1h Mass Coin Rush': [610, 0, 0],
    '30m Mass Supply Rush': [450, 0, 0],
    '100% Supply Boost': [320, 0, 0],
    '100% Coin Boost': [320, 0, 0],
    '50% Supply Boost': [160, 0, 0],
    '50% Coin Boost': [160, 0, 0],
    '10% Attacker Boost': [810, 0, 0],
    '5% Attacker Boost': [410, 0, 0],
    '30% Defender Boost': [270, 0, 0],
    '20% Defender Boost': [140, 0, 0],
    'Premium Decoration': [540, 0, 1],
    'Residential Building': [200, 0, 0],
    'Production Building': [200, 0, 0],
    'Cultural Building': [200, 0, 0],
    'Fragment of Shrine of Knowledge': [6750.0/15, 2.0/15, 3.0/15],
    'Fragment of Wishing Well': [5940.0/15, 1.0/15, 3.0/15],
    'Fragment of Wishing Well Shrink Kit': [4050.0/15, 3.0/15, 6.0/15]
}